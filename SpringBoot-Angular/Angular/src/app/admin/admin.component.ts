//Npm dependencies
import { Component, OnInit } from '@angular/core';

//Service imports
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})

/**
 * This component is admin dashboard
 */
export class AdminComponent implements OnInit {
  users:any;
  constructor(private service : ServiceService) { }

  ngOnInit() {
    /**
     * Calling service method for getting users .
     */
    this.service.getUsers().subscribe((data:any) => {
      this.users = data.response;
    })
  }
  
  /**
   * This method is to delete the user based on id
   * @param data 
   */
  delete(data){
    /**
     * Calling service method for deleting the user based on id.
     */
    this.service.deleteUser(data).subscribe((status:any) => {
      if(status.statusCode == 1){
        alert("deleted successfully");
        window.location.reload();
      }
    })
  }

}
