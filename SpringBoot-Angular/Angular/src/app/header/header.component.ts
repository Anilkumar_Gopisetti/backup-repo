// npm sependencies
import { Component, OnInit }        from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

/**
 * This component is header component.
 */
export class HeaderComponent implements OnInit {
  constructor() { }
  isLogin: boolean = false;

  ngOnInit() {
    // Checking whether id is present in local storage or not.
    if (localStorage.getItem('id')) {
      this.isLogin = true;
    }
  }

  signOut() {
    if (window.confirm("Do you want to logout")) {
      //Removing id from local storage 
      localStorage.removeItem('id');
      //Redirecting to base page
      window.location.href = "/";
    }
  }
}
