//Npm dependencies
import { Component, OnInit }       from '@angular/core';

//Service imports
import { ServiceService }          from '../service.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

/**
 * This component is about displaying the user details.
 */
export class UserComponent implements OnInit {
  userData: any;

  constructor(private service: ServiceService) { }

  ngOnInit() {
    /**
     * Calling service method for getting user details based on id.
     */
    this.service.getUserById().subscribe((data: any) => {
      //Storing the response object in UserData.
      this.userData = data.response;
    })
  }
}
