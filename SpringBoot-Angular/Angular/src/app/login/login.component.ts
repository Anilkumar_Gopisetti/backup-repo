// Npm dependencies
import { Component, OnInit }                      from '@angular/core';
import { Router }                                 from '@angular/router';
import { FormGroup, FormBuilder, Validators }     from '@angular/forms';

// Service imports
import { ServiceService }                         from '../service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

/**
 * This component is for student Login.
 */
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  email_status: string;
  password_status: string;
  userData : any;

  constructor(private formBuilder: FormBuilder, private service: ServiceService, private router: Router) { }

  ngOnInit() {
    /**
     * Validations for login fields.
     */
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  valuechange(newValue) {
    this.password_status = " ";
    this.email_status = " ";
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    /**
     * Calling service method by sending the form details.
     */
    this.service.doLogin(this.loginForm.value).subscribe((data) => {
      if(data.statusCode == 1){
        //Checking for user role
        if(data.response.role == 1){
          //Storing user Id in localstorage
          localStorage.setItem('id', data.response.userId);
          //Navgating to admin page
          window.location.href = "/admin";
        }
        else if(data.response.role == 2){
          //Storing user Id in localstorage
          localStorage.setItem('id', data.response.userId);
          //Navgating to user page
          window.location.href = "/user";
        }
        else if(data.response.role == null){
          //Storing user Id in localstorage
          localStorage.setItem('id', data.response.userId);  
          //Navgating to user page     
          this.router.navigate(['/user']);
        }
      }
      else{
        alert("please enter correct credentials")
      }
    })
  }
}