//Npm dependencies
import { Component, OnInit }                    from '@angular/core';
import { Router }                               from '@angular/router';
import { FormBuilder, FormGroup, Validators }   from '@angular/forms';


//Service imports
import { ServiceService }                       from '../service.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

/**
 * This component is for student registration.
 */
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  errorEmail: string;

  constructor(private formBuilder: FormBuilder, private service: ServiceService, private routes: Router) { }
  
  ngOnInit() {
    /**
     * Validations for registration fields.
     */
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.minLength(10)]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() { 
    return this.registerForm.controls; 
  }
  /**
   * Registration form submission.
   */
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    /**
     * Calling service method by sending the form details.
     */
    this.service.doRegister(this.registerForm.value).subscribe((data) => {
      if (data.statusCode == 1) {
        alert("Student details saved successfully...!");
        //Navigating to base page
        this.routes.navigate(['/']);
      }
      else if (data.statusCode == -2) {
        this.errorEmail = "email id already exists"
        alert(this.errorEmail);
      }
    })
  }
}
