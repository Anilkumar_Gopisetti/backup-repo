//Npm dependencies
import { Component, OnInit }      from '@angular/core';
import { FormGroup }              from '@angular/forms';
import { Router }                 from '@angular/router';

//Service imports
import { ServiceService }         from '../service.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})

/**
 * This component is used to edit the details of users
 */
export class EditUserComponent implements OnInit {
  userData: any;
  updateData: any;
  userid: any;
  name: string;
  email: string;
  password: string;
  phone: string;
  editForm: FormGroup;
  constructor(private service: ServiceService, private routes: Router) { }

  ngOnInit() {
    //Calling service method for getting the user details by id.
    this.service.getUserById().subscribe((data) => {
      this.userData = data.response;
    })
  }

  /**
   *This method is to Update the user details. 
   */
  update() {
    /**
     * Calling service method by sending the updated details of student.
     */
    this.service.editUser(this.userData).subscribe((data: any) => {
      if (data.statusCode == 1) {
        alert("user details updated successfully");
        //Navigating to users page.
        this.routes.navigate(["/user"])
      }
    })
  }
}
