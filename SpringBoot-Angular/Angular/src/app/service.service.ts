//Npm dependencies
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

/**
 * This is service component and api calls takes place here.
 */
export class ServiceService {
  //Storing the userId from local storage.
  userId:String = localStorage.getItem("id");
  
  //assigning the url path to a variable
  url = "http://192.168.150.70:8080/api/"
  constructor(private http:HttpClient) { }

  /**
   * This method is for login.
   * @param loginData 
   */
  doLogin(loginData){
    return this.http.post<any>(this.url + 'user/loginUser',loginData);
  }

 /**
   * This method is for registration.
   * @param loginData 
   */
  doRegister(registerData){
    return this.http.post<any>(this.url+ "user/registerUser",registerData);
  }

  /**
   * This method is used to get the users.  
   */
  getUsers(){
    return this.http.get(this.url+ 'admin/displayUsers')
  }

  /**
   * This method is used to get the user details by id.  
   */
  getUserById(){
    return this.http.post<any>(this.url + 'user/getUserById/'+ this.userId,null)
  }

  /**
   * This method is used to delete the user details by id. 
   * @param data 
   */
  deleteUser(data){
    let body:any={
      email:data
    };
    return this.http.post<any>(this.url + 'admin/deleteUser', body)
  }

  /**
   * This method is used to edit the user details by id. 
   * @param data 
   */
  editUser(data){
    return this.http.put(this.url + 'user/updateUser', data)
  }
}
