package com.springibatisdemoapp.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springibatisdemoapp.entities.User;
import com.springibatisdemoapp.services.UserService;
import com.springibatisdemoapp.util.ResponseObject;

/**
 * This class refers to user controller which creates api's for user services.
 * @author IMVIZAG
 *
 */
@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private ResponseObject responseObject;
	
	
	
	/**
	 * this method is used for registering the user
	 * @param user
	 * @return
	 */
	@PostMapping("/registerUser")
	public ResponseEntity<ResponseObject> registerUser(@RequestBody User user){
		
		ResponseEntity<ResponseObject> responseEntity = null;
		//verfying that email is already existed or not
		User userByEmail = userService.isExisted(user.getEmail());
		boolean isRegistered = false;
		
		if(userByEmail == null) {
			isRegistered = userService.save(user);
			if(isRegistered) {
				
			responseObject.setResponse("Registered Successfully");
			responseObject.setStatusCode(1);
			}else {
				responseObject.setResponse("Something wrong");
				responseObject.setStatusCode(-1);
			}
		}else {
			responseObject.setResponse("user with this Email already exists");
			responseObject.setStatusCode(-2);
		}
  		//adding response object  and status code of response entity class

		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
		
		
		
	}
	
	/**
	 * this method is used to login either user or admin
	 * @param user
	 * @return
	 */
	@PostMapping("/loginUser")
	public ResponseEntity<ResponseObject> loginUser(@RequestBody User user){
		
		ResponseEntity<ResponseObject> responseEntity = null;
		String email = user.getEmail();
		String password = user.getPassword();
		User verifyUser = userService.verifyUser(email,password);
		
		if(verifyUser!=null)
		{
			responseObject.setResponse(verifyUser);
			responseObject.setStatusCode(1);
		}else {
			responseObject.setResponse("Login failed");
			responseObject.setStatusCode(-1);
		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
		
		}

	 /**
	  * this method is used for updating the profile of the user 
	  * @param user
	  * @return
	  */
	@PutMapping("/updateUser")
	@CrossOrigin
	public ResponseEntity<ResponseObject> updateUser(@RequestBody User user){
		
		ResponseEntity<ResponseObject> responseEntity = null;
		User userByEmail = userService.isExisted(user.getEmail());
		User updatedUser = userService.update(user);
		if(userByEmail != null) {
			updatedUser = userService.update(user);
			if(updatedUser!=null) {
			responseObject.setResponse(updatedUser);
			responseObject.setStatusCode(1);
			}else {
				responseObject.setResponse("Something wrong");
				responseObject.setStatusCode(-1);
			}
		}else {
			responseObject.setResponse("user with this Email does not exists");
			responseObject.setStatusCode(-2);
		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
		
	}
	
	/**
	 * this method is used to get the details of the particular user by using the id
	 * @param userId
	 * @return
	 */
	@PostMapping("/getUserById/{userId}")
	public ResponseEntity<ResponseObject> getUserById(@PathVariable("userId") int userId){
		
		ResponseEntity<ResponseObject> responseEntity = null;
		User userById = userService.getUserById(userId);
		
		if(userById != null) {
			
			responseObject.setResponse(userById);
			responseObject.setStatusCode(1);
		}else {
			
			responseObject.setResponse("User with this ID does not exists");
			responseObject.setStatusCode(-1);
			
		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
	}
	
		

}
