package com.springibatisdemoapp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.springibatisdemoapp.entities.User;

/**
 * this interface used to declare the methods to perform the database operations
 *
 */
@Mapper
public interface UserMapper {

	@Results({ @Result(property = "userId", column = "userId") })

	@Select("SELECT * FROM user WHERE email = #{email}")
	public User getUserByEmail(@Param("email") String email);

	@Insert("INSERT into user(name,password,email,phone) VALUES(#{name},#{password},#{email},#{phone})")
	int insertUser(User user);

	@Update("UPDATE user SET name=#{name}, phone =#{phone},email=#{email},password=#{password},role =#{role} WHERE userId =#{userId}")
	int updateUser(User user);

	@Select("SELECT * FROM user WHERE userId = #{userId}")
	public User getUserById(@Param("userId") int userId);

	@Select("SELECT * FROM user")
	public List<User> getAllUsers();

	@Delete("DELETE FROM user WHERE userId =#{userId}")
	int deleteUserByUserId(int userId);

}
