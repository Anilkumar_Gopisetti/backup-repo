package com.springibatisdemoapp;

import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.springibatisdemoapp.entities.User;

@MappedTypes(User.class)
@MapperScan("com.springibatisdemoapp.mapper")
@SpringBootApplication

public class SpringIbatisDemoAppApplication {

	
	
	public static void main(String[] args) {
		SpringApplication.run(SpringIbatisDemoAppApplication.class, args);
		
		
	}

	
}
