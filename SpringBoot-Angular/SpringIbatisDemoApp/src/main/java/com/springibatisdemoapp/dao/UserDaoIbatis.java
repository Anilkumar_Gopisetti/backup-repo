package com.springibatisdemoapp.dao;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.springibatisdemoapp.entities.User;

@Repository
public class UserDaoIbatis implements UserDao {

	public User addUser(User user, SqlMapClient sqlmapClient) {
        try
        {
        	Integer id = (Integer)sqlmapClient.queryForObject("user.getMaxUserId");
            id = id == null ? 1 : id + 1;
            user.setUserId(id);
            sqlmapClient.insert("user.addUser", user);
            user = getUserByEmail(user.getEmail(), sqlmapClient);
            return user;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
 
    @Override
    public User getUserByEmail(String email, SqlMapClient sqlmapClient) {
        try
        {
            User user = (User)sqlmapClient.queryForObject("user.getUserByEmail", email);
            return user;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
 
    @Override
    public boolean deleteUserById(Integer id, SqlMapClient sqlmapClient) {
        try
        {
            int result = sqlmapClient.delete("user.deleteUserByuserId", id);
            if(result == 1) 
            	return true;
            else
            	return false;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
		return false;
    }

	@Override
	public Map<User, List> getAllUsers(SqlMapClient sqlmapClient) {
		
		  Map<User, List> result = new LinkedHashMap<User, List>();
		//List<User> allUsers = new ArrayList<>();
		System.out.println(result.size()+"--------------------------------------------");
		try {
			result = (Map<User, List>) sqlmapClient.queryForObject("user.getAllUsers");
		
		} catch(Exception e)
        {
            e.printStackTrace();
        }
		
		return result;
	}

	@Override
	public User updateUser(User user,SqlMapClient sqlmapClient) {
		
	String phone = user.getPhone();
	String password = user.getName();
		
		//User userUpdated = sqlmapClient.update("user.updateUser", user);
      
        return user;
	}
	

}
