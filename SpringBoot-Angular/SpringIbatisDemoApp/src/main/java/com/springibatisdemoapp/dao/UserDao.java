package com.springibatisdemoapp.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.springibatisdemoapp.entities.User;

@Component
public class UserDao {
	
	@Autowired
	private SqlSession sqlSession;

	public User getUserByEmail(String  email) {
		return this.sqlSession.selectOne("getUserByEmail", email);
	}

	public int deleteUserByUserId(int userId) {
		return this.sqlSession.delete("deleteUserByUserId", userId);
	}
	
	public int insertUser(User user) {
		return this.sqlSession.insert("insertUser", user);
	}
	public List<User> getAllUsers() {
		return this.sqlSession.selectList("getAllUsers");
	}
	public int updateUser(User user) {
		return this.sqlSession.update("updateUser",user);
	}
	
	public User getUserById(int  userId) {
		return this.sqlSession.selectOne("getUserById", userId);
	}
}
