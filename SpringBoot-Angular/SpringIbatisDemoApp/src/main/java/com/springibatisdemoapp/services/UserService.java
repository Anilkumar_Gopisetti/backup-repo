package com.springibatisdemoapp.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springibatisdemoapp.entities.User;
import com.springibatisdemoapp.mapper.UserMapper;

/**
 * This class refers to User service which calls the methods from UserDAO
 * @author IMVIZAG
 *
 */
@Service
@Transactional
public class UserService {

	@Autowired
	private UserMapper userDao;
	
	/**
	 * this method is used to verify the user email wethe it is already existed or not
	 * @param email
	 * @return
	 */
	public User isExisted(String email) {

		User user = userDao.getUserByEmail(email);
        
        	if(user != null) 
        		return user;
        	else 
        		return null;
	

}
	/**
	 * this method is used to save the user object in the database
	 * @param user
	 * @return
	 */
	public boolean save(User user) {
		
	        int result = userDao.insertUser(user);
		 if(result!=0)
		 {
			 
			 return true;
		 }
		 else
			 return false;
	}
	
	/***
	 * this method is used to update the profile information of the particular user
	 * @param profile
	 * @return
	 */
	public User update(User profile) {
		
		
		 User getUser = isExisted(profile.getEmail());
		
		
		if(!getUser.getName().equals(profile.getName()))
		{
			getUser.setName(profile.getName());
		}
		
		if(!getUser.getPhone().equals(profile.getPhone()))
		{
			getUser.setPhone(profile.getPhone());
		}
		if(!getUser.getPassword().equals(profile.getName()))
		{
			getUser.setName(profile.getName());
		}
		int updatedUser = userDao.updateUser(getUser);
		if(updatedUser!=0)
		return getUser;
		else
			return null;
	}

		
		

	

	 /**
	  * this method is used to verify the user for login purpose
	  * @param email
	  * @param password
	  * @return
	  */
	public User verifyUser(String email, String password) {
		
		
		User user = isExisted(email);
		if(user !=null) {
			
			if(user.getPassword().equals(password))
				return user;	
		}
		
		return null;
		
	}
	
/**
 * this method is used to get the user object for the data base by using the userId
 * @param userId
 * @return
 */
	
	public User getUserById(int userId) {

		User user = userDao.getUserById(userId);
        
        if(user != null) {
        	return user;
	}else {
		return null;
	}

	}

}
