package com.springibatisdemoapp.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springibatisdemoapp.entities.User;
import com.springibatisdemoapp.mapper.UserMapper;

/**
 * This class refers to admin service which calls the methods from AdminDAO
 * 
 * @author IMVIZAG
 *
 */
@Service
@Transactional
public class AdminService {

		@Autowired(required = true)
	private UserMapper userDao;

		  
/**
 *  this method is refering to the  display all user which was called by controller class 

 * @return
 */
	public List<User> displayAllUsers() {

		List<User> allUsers = userDao.getAllUsers();
		return allUsers;

	}

	/**
	 *this method is refering to the  deleteUser which was called by delete user method in controller class
	 * @param id
	 * @return
	 */
	public boolean deleteUser(int id) {

		int result = userDao.deleteUserByUserId(id);

		if (result != 0) {
			return true;
		}
		return false;
	}

}
