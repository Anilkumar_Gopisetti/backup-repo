import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  email_status: string;
  password_status: string;
  userData : any;

  constructor(private formBuilder: FormBuilder, private service: ServiceService, private router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  valuechange(newValue) {
    this.password_status = " ";
    this.email_status = " ";
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.service.doLogin(this.loginForm.value).subscribe((data) => {
      console.log(data)
      if(data.statusCode == 1){
        if(data.response.role == "admin"){
          localStorage.setItem('id', data.response.username);
          // this.router.navigate(['/admin']);
          window.location.href = "/admin";
          
        }
        else if(data.response.role == "user"){
          localStorage.setItem('id', data.response.username);
          window.location.href = "/user";
          // this.router.navigate(['/user']);
 
        }
        else if(data.response.role == null){
          localStorage.setItem('id', data.response.username);          
          this.router.navigate(['/user']);
        }
      }
      else{
        alert("please enter correct credentials")
      }
    })
  }
}

