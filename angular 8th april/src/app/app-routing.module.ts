import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { UserComponent } from './user/user.component';
import { EditUserComponent } from './edit-user/edit-user.component';

//Route paths
const routes: Routes = [
  {path : '',            component : LoginComponent},
  {path : 'register',    component : RegisterComponent },
  {path : 'admin',       component : AdminComponent},
  {path : 'user',        component : UserComponent},
  {path : 'editUser',        component : EditUserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
