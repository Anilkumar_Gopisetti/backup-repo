import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }
  isLogin:boolean=false;
  ngOnInit() { 
    if(localStorage.getItem('id')){
      this.isLogin = true;
    }
  }
  signOut(){
   if(window.confirm("do you wanna logout")){
     localStorage.removeItem('id');
     window.location.href="/";
   }
  }

}
