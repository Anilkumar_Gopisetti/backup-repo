import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from '../service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm : FormGroup;
  submitted = false;
  errorEmail:string;

  constructor(private formBuilder : FormBuilder,private service : ServiceService, private routes : Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(6)]],
      username: ['', [Validators.required, Validators.minLength(3)]],
      organization: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.minLength(10)]],
      password : ['', [Validators.required, Validators.minLength(3)]],
      });
  }
  get f() { return this.registerForm.controls; }
  onSubmit(){
    this.submitted = true;
    if(this.registerForm.invalid){
      return;
    }
    this.service.doRegister(this.registerForm.value).subscribe((data) => {
      console.log(data);
      if(data.statusCode == 1){
        alert("Student details saved successfully...!");
        this.routes.navigate(['/']);
      }
      else if (data.statusCode == -2){
        this.errorEmail = "email id already exists"
        alert(this.errorEmail);
      }
    })
  } 
}
