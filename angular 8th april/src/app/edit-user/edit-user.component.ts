import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})

export class EditUserComponent implements OnInit {
 userData :any;
 updateData:any;
 userid:any;
 name:string;
 email:string;
 password:string;
 phone:string;
editForm : FormGroup;
  constructor(private service : ServiceService, private routes : Router) { }

  ngOnInit() {
    this.service.getUserById().subscribe((data)=>{
      this.userData = data.response;
    })
  }

update(){
this.service.editUser(this.userData).subscribe((data:any) => {
 console.log(data);
  if (data.statusCode == 1){
    alert("user details updated successfully");
    this.routes.navigate(["/user"])
  }
})

}
}
