import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  users:any;
  constructor(private service : ServiceService) { }

  ngOnInit() {
    this.service.getUsers().subscribe((data:any) => {
      this.users = data.response;
      console.log(this.users)
    })
  }
  
  delete(data){
    this.service.deleteUser(data).subscribe((status:any) => {
      console.log(status);
      if(status.statusCode == 1){
        alert("deleted successfully");
        window.location.reload();
      }
    })
  }

}
