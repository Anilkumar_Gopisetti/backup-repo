import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  userName:String = localStorage.getItem("id");
  url = "http://192.168.150.51:8080/api/"
  constructor(private http:HttpClient) { }

  doLogin(loginData){
    return this.http.post<any>(this.url + 'user/loginUser',loginData);
  }

  doRegister(registerData){
    return this.http.post<any>(this.url+ "user/registerUser",registerData);
  }

  getUsers(){
    return this.http.get(this.url+ 'admin/getAllUsers')
  }

  getUserById(){
    return this.http.get<any>(this.url + 'user/getUserByUsername/'+ this.userName)
  }

  deleteUser(data){
    // let body:any={
    //   username:data
    // };
    // console.log(body)
    return this.http.delete<any>(this.url + 'admin/deleteUser' + this.userName)
  }
  editUser(data){
    return this.http.put(this.url + 'user/updateUser', data)
  }
}
