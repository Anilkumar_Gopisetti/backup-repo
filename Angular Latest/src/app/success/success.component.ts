import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {
  userName : string;
  constructor(private service: ServiceService) { }

  ngOnInit() {
    this.userName = this.service.getUserName();
  }

}
