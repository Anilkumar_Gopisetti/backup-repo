import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { RegisterComponent } from './register/register.component';
import { AboutComponent } from './about/about.component';
import { AllusersComponent } from './allusers/allusers.component';
import { SearchComponent } from './search/search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SortingComponent } from './sorting/sorting.component';
import { SortPipe } from './sort.pipe';
import { SuccessComponent } from './success/success.component';
import { MessagesComponent } from './messages/messages.component';
import { ShowMessageComponent } from './show-message/show-message.component';




@NgModule({
  declarations: [
    AppComponent,
    MainpageComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    RegisterComponent,
    AboutComponent,
    AllusersComponent,
    SearchComponent,
    SortingComponent,
    SortPipe,
    SuccessComponent,
    MessagesComponent,
    ShowMessageComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
