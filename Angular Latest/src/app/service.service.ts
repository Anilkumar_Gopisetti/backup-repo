import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  url : string = "http://192.168.150.97:8080/api/";
  msgData=[];
  sortedNames = [];
  userName : string = "";
  constructor(private http : HttpClient) { }

  doregistration (data) {
    return this.http.post<any>(this.url +"user/registerUser ",data);
  }

  search(data){
    return this.http.get<any>(this.url + "user/getUserByusername/"+ data)
  }

  getAllUsers(){
    return this.http.get<any>(this.url + "user/getAllUsers");
  }

  sendMessage(data:any)
  {
    this.msgData.push(data);
  }
  getMessage()
  {
    return  this.msgData;
  }
  getName(data){
    this.sortedNames.push(data);
    console.log(data);
  }
  sendNames(){
    return this.sortedNames;
  }
  sendUserName(name){
    console.log(name);
    this.userName = name;
  }
  getUserName(){
    console.log(this.userName)
    return this.userName;
  }
}
