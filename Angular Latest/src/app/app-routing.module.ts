import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent } from './mainpage/mainpage.component';
import { AboutComponent } from './about/about.component';
import { RegisterComponent } from './register/register.component';
import { AllusersComponent } from './allusers/allusers.component';
import { SearchComponent } from './search/search.component';
import { SuccessComponent } from './success/success.component';
import { MessagesComponent } from './messages/messages.component';

const routes: Routes = [
  {path : "", component : MainpageComponent},
  {path : "about", component : AboutComponent},
  {path : "register", component : RegisterComponent},
  {path : "users", component : AllusersComponent},
  {path : "search", component : SearchComponent},
  {path : "success", component : SuccessComponent},
  {path : "message", component : MessagesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
