import { Component, OnInit, Output } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-allusers',
  templateUrl: './allusers.component.html',
  styleUrls: ['./allusers.component.css']
})

export class AllusersComponent implements OnInit {
   public userData : any;
   sortedData  = new Array();
  constructor(private service : ServiceService) { }

  ngOnInit() {
    this.service.getAllUsers().subscribe((data) => {
      console.log(data);
      console.log(data.response);
      this.userData = data.response;
     
      for(let i=0;i<data.response.length;i++)
      {
   
        this.sortedData.push(data.response[i].firstName);
      }
      this.sortedData.sort();
      console.log(this.sortedData);
      this.service.getName(this.sortedData);
    })
  }

}
