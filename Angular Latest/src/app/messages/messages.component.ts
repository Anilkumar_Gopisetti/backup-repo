import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  @ViewChild("form")  messageFrom : NgForm;
  constructor( private service:ServiceService, private router : Router) { }

  ngOnInit() {
    
  }
  sendMessage(){
    console.log(this.messageFrom.value);
    this.service.sendMessage(this.messageFrom.value);
    this.router.navigate(["/"])
  }
}
