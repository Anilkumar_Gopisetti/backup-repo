import { Component, OnInit, Input } from '@angular/core';
import { ServiceService } from '../service.service';
import { splitAtColon } from '@angular/compiler/src/util';

@Component({
  selector: 'app-sorting',
  templateUrl: './sorting.component.html',
  styleUrls: ['./sorting.component.css']
})
 
export class SortingComponent implements OnInit {
  sortedData = new Array();
  constructor(private service : ServiceService) { }
  
  ngOnInit() {
    this.sortedData = this.service.sendNames();
    // this.sortedData.split(',');
    console.log(this.sortedData)
  }

}
