import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
@Component({
  selector: 'app-show-message',
  templateUrl: './show-message.component.html',
  styleUrls: ['./show-message.component.css']
})
export class ShowMessageComponent implements OnInit {
  msgData:any;
  constructor( private service:ServiceService) { }

  ngOnInit() {
    this.msgData=this.service.getMessage();
    console.log(this.msgData);
  }

}
