import { Component, OnInit, Input } from '@angular/core';
import { Form } from '@angular/forms';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  username : any;
  userDetails : any;
  noUserFound = false;
  constructor( private service : ServiceService ) { }

  ngOnInit() {
  }
  search(){
    this.service.search(this.username).subscribe((data) => {
      if(data.statusCode == 1){
      this.userDetails = data.response;
      this.noUserFound = false;
      }
      else if (data.statusCode == -1){
        this.noUserFound = true;
      }
    })
  }
}
