
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
	<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
<%@ include file="Assets/css/register.css"%>
</style>
</head>
<body>
	<form method="post"
		action="${contextPath}/user/editDetails/updatedetails">

		<div class="form-group" id="forms">
			<label class="field">Enter user name to update : </label> <input
				type="text" name="username" class="form-control field-id"
				placeholder="Enter Username" /><br /> <br /> <br /> <input
				type="hidden" name="context" value="searchforedit" />
			<button class="login" type="submit">Search</button>
		</div>

		<c:if test="${userDetails.username != null }">
			<jsp:forward page="updatedetails.jsp"></jsp:forward>
		</c:if>
		<c:if test="${userDetails.username == null && attempt>0}">
			<h4>No user found</h4>
		</c:if>
	</form>
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
</body>
</html>