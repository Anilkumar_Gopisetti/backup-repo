
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
    <c:set var = "userDetails" value = "${requestScope.userDetails}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style><%@include file="Assets/css/register.css"%></style>
</head>
<body>
<h1 style  = "color : white; font-size : 110%; margin-left : 7% ">Enter Details To Update</h1>
<form  method="post"action="${contextPath}/user/editDetails/updateDetails">
<table>
<tr>
				<td class = "field">User Id</td>
				<td><input class = "field-id" type="number"  name="id" value="${userDetails.id }" readonly="readonly"/></td>
			</tr>
			<tr>
				<td class = "field">First Name</td>
				<td><input type="text" class = "field-id" name="first_name" value="${userDetails.first_name }"/></td>
			</tr>
			<tr>
				<td class = "field">Last Name</td>
				<td><input type="text" class = "field-id" name="last_name" value="${userDetails.last_name }" /></td>
			</tr>
			<tr>
				<td class = "field">User Name</td>
				<td><input type="text" class = "field-id" name="username" value="${userDetails.username }"/></td>
			</tr>
			<tr>
				<td class = "field">Password</td>
				<td><input type="password" class = "field-id" name="password" value="${userDetails.password }"/></td>
			</tr>
			<tr>
				<td class = "field">Organization</td> 
				<td><input type="text" class = "field-id" name="organization" value="${userDetails.organization }" /></td>
			</tr>
			<tr>
				<td class = "field">Email</td>
				<td><input type="text" class = "field-id" name="email" value="${userDetails.email }"/></td>
			</tr>
			<tr>
				<td class = "field">Phone</td>
				<td><input type="text" class = "field-id" name="phone" value="${userDetails.phone }"/></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" class = "login" value="Update" /></td>
			</tr>
			</tr>
			    <td><input type="hidden" class = "login" name="context" value="updateDetails"/></td>
			
			<tr>
		</table>
</form>
<c:if test="${result eq true }">
<h4>Updated Successfully</h4>
</c:if>
<c:if test="${result eq false && attempt>0 }">
<h4>Updation Failed</h4>
</c:if>
</body>
</html>