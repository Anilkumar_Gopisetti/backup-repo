

<html>
<head>
  <!-- Bootstrap CDN links -->
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
  <!-- Font Awesome CDN link -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
  crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
 <!--  <link rel = "stylesheet" type = "text/css" href = "./header.css" />  -->
 	<style><%@include file="WEB-INF/jsp/Assets/css/header.css"%></style>
</head>


<body>
<div>
  <nav class="navbar navbar-expand" id="headerStyle"> 
      <div class="container-fluid" >
          <div class="navbar-header">
              
              <a style = "color : #00ba9d, font-size : 110% !important">Aspire Info Labs</a>

          </div>
          <ul class="navbar-nav navbar-right"  >
              <li class="nav-item" ><a class="nav-link" href="home/about">About</a></li>
              <li class="nav-item" ><a class="nav-link" href="user/registration/regform">Register</a></li>
              <li class="nav-item" ><a class="nav-link" href="user/editDetails/edit">Update Details</a></li>
              <li class="nav-item" ><a class="nav-link" href="user/delete/form">Delete User</a> </li>
          </ul>
      </div>
  </nav>
</div>
</body>
</html>