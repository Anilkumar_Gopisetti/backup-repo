package com.abc.util;

import java.io.IOException;
import java.io.Reader;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * This Class gives SqlMapClient class object
 * @author IMVIZAG
 *
 */
public class SqlMapClientObj {

	/**
	 * This Method reads database configuration information from xml file 
	 * and returns SqlMapClient object
	 * @return SqlMapClient object
	 */
	public static SqlMapClient getSqlMapClient() {
		Reader rd = null;
		try {
			//reading databse configuration information from xml file
			rd = Resources.getResourceAsReader("sqlmap_config.xml");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//creation of SqlMapClient object
		SqlMapClient smc = SqlMapClientBuilder.buildSqlMapClient(rd);
		return smc;
	}
}
