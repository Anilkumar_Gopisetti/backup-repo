package com.abc.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.abc.bean.UserDetails;
import com.abc.service.ProfileService;


public class UserController extends SimpleFormController {

	private ProfileService profileService;

	public ProfileService getProfileService() {
		return profileService;
	}

	public void setProfileService(ProfileService profileService) {
		this.profileService = profileService;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,	HttpServletResponse response, Object command,	BindException errors){
		ModelAndView view = null;
		if(request.getParameter("context").equalsIgnoreCase("registration")) {
			view  = saveUser(command);
		}else if(request.getParameter("context").equalsIgnoreCase("searchforedit")){
			view =searchForEdit(command);
		}

		else if(request.getParameter("context").equalsIgnoreCase("updateDetails")){
			view =updateDetails(command);
		}
		else if(request.getParameter("context").equalsIgnoreCase("delete")){
			view =deleteDetails(command);
		}
		return view;
	}

	private ModelAndView deleteDetails(Object command) {
		UserDetails user = (UserDetails) command;
		int userdeatils = profileService.deleteProfile(user.getUsername());
		String shwMsg = "No User found to delete";
		if(userdeatils !=0) {
			shwMsg ="deleted successfully";
		}
		return new ModelAndView("success","showmsg",shwMsg);
	}

	public ModelAndView saveUser(Object command) {
		UserDetails user = (UserDetails) command;
		UserDetails userdeatils = profileService.saveProfile(user);
		String shwMsg = "some thing went wrong";
		if(userdeatils != null) {
			shwMsg ="Registered successfully";
		}
		return new ModelAndView("success","showmsg",shwMsg);
	}


	public ModelAndView searchForEdit(Object Command) {
		
		System.out.println("varun");

		UserDetails user = (UserDetails) Command;
		UserDetails userdeatils = profileService.searchProfile(user.getUsername());

		return new ModelAndView("updatedetails","userDetails",userdeatils);
	}


	public ModelAndView updateDetails(Object command) {

		UserDetails user = (UserDetails) command;

		UserDetails result = profileService.updateDetails(user);
		String msg=null;
		if(result==null) {
			msg="updation failed";
		}
		else {
			msg=result.getUsername()+ " updated successfully";
		}

		return new ModelAndView("success","showmsg",msg);
	}
	
		
	
		


}
