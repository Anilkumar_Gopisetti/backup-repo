package com.abc.bean;

public class UserDetails {
	
	@Override
	public String toString() {
		return "UserDetails [firstName=" + first_name + ", lastName=" + last_name + ", username=" + username
				+ ", password=" + password + ", organization=" + organization + ", email=" + email + ", phone=" + phone
				+ "]";
	}
	private int id;
	private String first_name;
	private String last_name;
	private String username;
	private String password;
	private String organization;
	private String email;
	private String phone;
	
	
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	

}
