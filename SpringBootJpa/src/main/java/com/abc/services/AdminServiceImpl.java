package com.abc.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.abc.bean.UserDetails;
import com.abc.dao.UserDao;


/**
 * This class refers to admin service which calls the methods from AdminDAO
 * 
 * @author IMVIZAG
 *
 */
@Service
@Transactional
public class AdminServiceImpl implements AdminService{

	@Autowired
	private UserDao userDao;

		  
	/**
	 * this method is referring to the display all user which was called by
	 * controller class
	 * 
	 * @return
	 */
	public List<UserDetails> displayAllUsers() {

		List<UserDetails> userList = new ArrayList<>();
		
		userDao.findAll().forEach(userList::add);
		
		
		return userList;

	}


	/**
	 *this method is referring to the  deleteUser which was called by delete user method in controller class
	 * @param id
	 * @return
	 */
	public boolean deleteUser(int id) {

		try {
			userDao.deleteById(id);
			return true;
		}catch(EmptyResultDataAccessException e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		
		
		
	}

}
