package com.abc.services;

import java.util.List;

import com.abc.bean.UserDetails;

public interface AdminService {

	public List<UserDetails> displayAllUsers();
	
	public boolean deleteUser(int id);
}
