package com.abc.services;


import com.abc.bean.UserDetails;

public interface UserService {

	public UserDetails getUserByUsername(String username);

	public UserDetails save(UserDetails user);

	UserDetails verifyUser(String email, String password);

	UserDetails update(UserDetails profile);
}
