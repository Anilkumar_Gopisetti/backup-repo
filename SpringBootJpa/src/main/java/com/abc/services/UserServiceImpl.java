package com.abc.services;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.bean.UserDetails;
import com.abc.dao.UserDao;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao userDao;

	/**
	 * this method is used to verify the user email whether it is already existed or
	 * not
	 * 
	 * @param email
	 * @return
	 */
	@Override
	public UserDetails getUserByUsername(String username) {

		UserDetails user = userDao.findByUsername(username);

		if (user != null)
			return user;
		else
			return null;

	}

	/**
	 * this method is used to save the user object in the database
	 * 
	 * @param user
	 * @return
	 */
	@Override
	public UserDetails save(UserDetails user) {

		UserDetails result = userDao.save(user);
		if (result != null) {

			return result;
		} else
			return null;
	}
	/**
	  * this method is used to verify the user for login purpose
	  * @param email
	  * @param password
	  * @return
	  */
	@Override
	public UserDetails verifyUser(String email, String password) {
		
		
		UserDetails user = userDao.findByEmail(email);
		if(user !=null) {
			
			if(user.getPassword().equals(password))
				return user;	
		}
		
		return null;
		
	}
	

	/***
	 * this method is used to update the profile information of the particular user
	 * @param profile
	 * @return
	 */
	@Override
	public UserDetails update(UserDetails profile) {
		
		
		UserDetails getUser = getUserByUsername(profile.getUsername());
		
		
		if(!getUser.getFirstName().equals(profile.getFirstName()))
		{
			getUser.setFirstName(profile.getFirstName());
		}
		if(!getUser.getLastName().equals(profile.getLastName()))
		{
			getUser.setLastName(profile.getLastName());
		}
		if(!getUser.getUsername().equals(profile.getUsername()))
		{
			getUser.setUsername(profile.getUsername());
		}
		
		if(!getUser.getPhone().equals(profile.getPhone()))
		{
			getUser.setPhone(profile.getPhone());
		}
		if(!getUser.getPassword().equals(profile.getPassword()))
		{
			getUser.setPassword(profile.getPassword());
		}
		UserDetails updatedUser = userDao.save(getUser);
		if(updatedUser!=null)
		return updatedUser;
		else
			return null;
	}


}
