package com.abc.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.bean.UserDetails;
import com.abc.services.UserService;
import com.abc.services.UserServiceImpl;
import com.abc.util.ResponseObject;

@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {

	@Autowired
	private UserService userService;


	/**
	 * this method is used for registering the user
	 * 
	 * @param user
	 * @return
	 */
	@PostMapping("/registerUser")
	public ResponseEntity<ResponseObject> registerUser(@RequestBody UserDetails user) {

		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		// verifying that email is already existed or not
		UserDetails userByName = userService.getUserByUsername(user.getUsername());
		UserDetails savedUser;

		if (userByName == null) {
			savedUser = userService.save(user);
			if (savedUser != null) {

				responseObject.setResponse(savedUser);
				responseObject.setStatusCode(1);
			} else {
				responseObject.setResponse("Something wrong");
				responseObject.setStatusCode(-1);
			}
		} else {
			responseObject.setResponse("user with this User Name already exists");
			responseObject.setStatusCode(-2);
		}
		// adding response object and status code of response entity class

		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;

	}
	/**
	 * this method is used to login either user or admin
	 * @param user
	 * @return
	 */
	@PostMapping("/loginUser")
	public ResponseEntity<ResponseObject> loginUser(@RequestBody UserDetails user){
		
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		String email = user.getEmail();
		String password = user.getPassword();
		UserDetails verifyUser = userService.verifyUser(email,password);
		
		if(verifyUser!=null)
		{
			responseObject.setResponse(verifyUser);
			responseObject.setStatusCode(1);
		}else {
			responseObject.setResponse("Login failed");
			responseObject.setStatusCode(-1);
		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
		
		}


	/**
	 * this method is used to get the details of the particular user by using the id
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("/getUserByUsername/{username}")
	public ResponseEntity<ResponseObject> getUserByUsername(@PathVariable("username") String username) {

		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		UserDetails userByName = userService.getUserByUsername(username);

		if (userByName != null) {

			responseObject.setResponse(userByName);
			responseObject.setStatusCode(1);
		} else {

			responseObject.setResponse("User with this User name does not exists");
			responseObject.setStatusCode(-1);

		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
	}

	}
