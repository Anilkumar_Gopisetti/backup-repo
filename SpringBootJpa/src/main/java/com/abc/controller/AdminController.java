package com.abc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.bean.UserDetails;
import com.abc.services.AdminService;
import com.abc.services.AdminServiceImpl;
import com.abc.services.UserService;
import com.abc.util.ResponseObject;


/**
 * This class refers to admin controller which creates api's for admin services.
 * 
 * @author IMVIZAG
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/api/admin")
public class AdminController {

	@Autowired
	private AdminService adminService;
	
	@Autowired
	private UserService userService;
	

/**
* this method is used for admin to see the all users who are registered
* 
* @return
*/
@GetMapping("/getAllUsers")
public ResponseEntity<ResponseObject> displayAllUsers() {
	
	ResponseEntity<ResponseObject> responseEntity = null;
	ResponseObject responseObject = new ResponseObject();
	List<UserDetails> userList = adminService.displayAllUsers();
	if (userList.size() != 0) {
		responseObject.setResponse(userList);
		responseObject.setStatusCode(1);
	} else {
		responseObject.setResponse("No User Found");
		responseObject.setStatusCode(-1);
	}
	// adding response object and status code of response entity class
	responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

	return responseEntity;
}


	/*
	 * this method is used for admin to delete the particular user
	 */
	@DeleteMapping("/deleteUser/{username}")
	public ResponseEntity<ResponseObject> deleteUsers(@PathVariable("username") String username) {
		ResponseObject responseObject = new ResponseObject();
		ResponseEntity<ResponseObject> responseEntity = null;
		UserDetails userByName = userService.getUserByUsername(username);

		if (userByName != null) {
			boolean isDeleted = adminService.deleteUser(userByName.getId());
			if (isDeleted) {
				responseObject.setResponse("Deleted successfully");
				responseObject.setStatusCode(1);
			} else {

				responseObject.setResponse("user deletion failed");
				responseObject.setStatusCode(-1);
			}
		} else {
			responseObject.setResponse("User with this Email does not exists");
			responseObject.setStatusCode(1);
		}

		// adding response object and status code of response entity class

		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		return responseEntity;

	}
}
