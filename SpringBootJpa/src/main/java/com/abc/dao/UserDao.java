package com.abc.dao;

import org.springframework.data.repository.CrudRepository;

import com.abc.bean.UserDetails;

public interface UserDao extends CrudRepository<UserDetails, Integer>{
	
	public UserDetails findByUsername(String username);
	
//	public UserDetails findById(int id);
	
	public UserDetails findByEmail(String email);
	
}
