package com.abc.util;

import java.util.Comparator;

import com.abc.bean.UserDetails;

public class SortByFirstName implements Comparator<UserDetails>{

	@Override
	public int compare(UserDetails obj1, UserDetails obj2) {
		// TODO Auto-generated method stub
		return obj1.getFirstName().compareTo(obj2.getFirstName());
	}

}
