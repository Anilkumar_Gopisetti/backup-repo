package com.abc.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.abc.util.SortByFirstName;
import com.abc.bean.UserDetails;
import com.abc.services.UserServiceImpl;
import com.abc.util.ResponseObject;

@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {

	@Autowired
	private UserServiceImpl userService;


	/**
	 * this method is used for registering the user
	 * 
	 * @param user
	 * @return
	 */
	@PostMapping("/registerUser")
	public ResponseEntity<ResponseObject> registerUser(@RequestBody UserDetails user) {

		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		// verfying that email is already existed or not
		UserDetails userByName = userService.getUserByUsername(user.getUsername());
		boolean isRegistered = false;

		if (userByName == null) {
			isRegistered = userService.save(user);
			if (isRegistered) {

				responseObject.setResponse("Registered Successfully");
				responseObject.setStatusCode(1);
			} else {
				responseObject.setResponse("Something wrong");
				responseObject.setStatusCode(-1);
			}
		} else {
			responseObject.setResponse("user with this Email already exists");
			responseObject.setStatusCode(-2);
		}
		// adding response object and status code of response entity class

		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;

	}

	/**
	 * this method is used to get the details of the particular user by using the id
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("/getUserByusername/{username}")
	public ResponseEntity<ResponseObject> getUserByUsername(@PathVariable("username") String username) {

		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		UserDetails userByName = userService.getUserByUsername(username);

		if (userByName != null) {

			responseObject.setResponse(userByName);
			responseObject.setStatusCode(1);
		} else {

			responseObject.setResponse("User with this User name does not exists");
			responseObject.setStatusCode(-1);

		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
	}

	/**
	 * this method is used for admin to see the all users who are registered
	 * 
	 * @return
	 */
	@GetMapping("/getAllUsers")
	public ResponseEntity<ResponseObject> displayAllUsers() {
		
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		List<UserDetails> userList = userService.displayAllUsers();
//		Collections.sort(userList, new SortByFirstName());
		if (userList.size() != 0) {
			responseObject.setResponse(userList);
			responseObject.setStatusCode(1);
		} else {
			responseObject.setResponse("No User Found");
			responseObject.setStatusCode(-1);
		}
		// adding response object and status code of response entity class
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		return responseEntity;
	}
}
