package com.abc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.abc.bean.UserDetails;

@Mapper
public interface UserMapper {

	@Insert("INSERT into user(firstName,lastName,username,organization,password,email,phone) VALUES(#{firstName},#{lastName},#{username},#{organization},#{password},#{email},#{phone})")
	int insertUser(UserDetails user);
	
	@Select("SELECT * FROM user WHERE username = #{username}")
	public UserDetails getUserByUsername(@Param("username") String username);
	
	@Select("SELECT * FROM user")
	public List<UserDetails> getAllUsers();
	
}
