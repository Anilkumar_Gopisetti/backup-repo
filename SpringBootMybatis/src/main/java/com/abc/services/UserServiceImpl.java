package com.abc.services;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.bean.UserDetails;
import com.abc.mapper.UserMapper;

@Service

public class UserServiceImpl {

	@Autowired
	private UserMapper userMapper;

	/**
	 * this method is used to verify the user email wethe it is already existed or
	 * not
	 * 
	 * @param email
	 * @return
	 */
	public UserDetails getUserByUsername(String username) {

		UserDetails user = userMapper.getUserByUsername(username);

		if (user != null)
			return user;
		else
			return null;

	}

	/**
	 * this method is used to save the user object in the database
	 * 
	 * @param user
	 * @return
	 */
	public boolean save(UserDetails user) {

		int result = userMapper.insertUser(user);
		if (result != 0) {

			return true;
		} else
			return false;
	}

	/**
	 * this method is refering to the display all user which was called by
	 * controller class
	 * 
	 * @return
	 */
	public List<UserDetails> displayAllUsers() {

		List<UserDetails> allUsers = userMapper.getAllUsers();
		return allUsers;

	}

}
