package com.abc.services;

import java.util.List;

import com.abc.bean.UserDetails;

public interface UserService {

	public UserDetails getUserByUsername(String username);

	public boolean save(UserDetails user);

	public List<UserDetails> displayAllUsers();
}
