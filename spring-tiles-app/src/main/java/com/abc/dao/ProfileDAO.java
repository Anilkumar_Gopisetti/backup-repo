package com.abc.dao;

import java.util.List;

import com.abc.bean.UserDetails;

public interface ProfileDAO {
	
	int createProfile(UserDetails userDetails);
	UserDetails getUserByUserName(String username);
	List<UserDetails> getAllUsers();

}
